<?php
	echo json_decode('{
   "geocoded_waypoints" : [
      {
         "geocoder_status" : "OK",
         "place_id" : "ChIJa147K9HX3IAR-lwiGIQv9i4",
         "types" : [ "amusement_park", "establishment", "point_of_interest", "political" ]
      },
      {
         "geocoder_status" : "OK",
         "place_id" : "ChIJzzgyJU--woARcZqceSdQ3dM",
         "types" : [ "amusement_park", "establishment", "point_of_interest" ]
      }
   ],
   "routes" : [
      {
         "bounds" : {
            "northeast" : {
               "lat" : 34.1394486,
               "lng" : -117.909663
            },
            "southwest" : {
               "lat" : 33.8068674,
               "lng" : -118.363625
            }
         },
         "copyrights" : "Map data ©2019",
         "legs" : [
            {
               "arrival_time" : {
                  "text" : "6:14am",
                  "time_zone" : "America/Los_Angeles",
                  "value" : 1566566075
               },
               "departure_time" : {
                  "text" : "3:55am",
                  "time_zone" : "America/Los_Angeles",
                  "value" : 1566557718
               },
               "distance" : {
                  "text" : "51.3 mi",
                  "value" : 82538
               },
               "duration" : {
                  "text" : "2 hours 19 mins",
                  "value" : 8357
               },
               "end_address" : "100 Universal City Plaza, Universal City, CA 91608, USA",
               "end_location" : {
                  "lat" : 34.1364652,
                  "lng" : -118.3569535
               },
               "start_address" : "1313 Disneyland Dr, Anaheim, CA 92802, USA",
               "start_location" : {
                  "lat" : 33.8094802,
                  "lng" : -117.9189716
               },
               "steps" : [
                  {
                     "distance" : {
                        "text" : "0.2 mi",
                        "value" : 387
                     },
                     "duration" : {
                        "text" : "5 mins",
                        "value" : 282
                     },
                     "end_location" : {
                        "lat" : 33.8099608,
                        "lng" : -117.9154365
                     },
                     "html_instructions" : "Walk to Disneyland",
                     "polyline" : {
                        "points" : "gljmEp`vnUCc@Uu@Ni@VUKu@M[GQ]gAg@iA\\_@@E@E?e@AaAFSAU?[?_@?k@?Mk@@"
                     },
                     "start_location" : {
                        "lat" : 33.8094802,
                        "lng" : -117.9189716
                     },
                     "steps" : [
                        {
                           "distance" : {
                              "text" : "148 ft",
                              "value" : 45
                           },
                           "duration" : {
                              "text" : "1 min",
                              "value" : 35
                           },
                           "end_location" : {
                              "lat" : 33.8096138,
                              "lng" : -117.9185229
                           },
                           "html_instructions" : "Head \u003cb\u003eeast\u003c/b\u003e",
                           "polyline" : {
                              "points" : "gljmEp`vnUCc@Uu@"
                           },
                           "start_location" : {
                              "lat" : 33.8094802,
                              "lng" : -117.9189716
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "128 ft",
                              "value" : 39
                           },
                           "duration" : {
                              "text" : "1 min",
                              "value" : 28
                           },
                           "end_location" : {
                              "lat" : 33.8094087,
                              "lng" : -117.9181956
                           },
                           "html_instructions" : "Turn \u003cb\u003eright\u003c/b\u003e",
                           "maneuver" : "turn-right",
                           "polyline" : {
                              "points" : "amjmEv}unUNi@VU"
                           },
                           "start_location" : {
                              "lat" : 33.8096138,
                              "lng" : -117.9185229
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "420 ft",
                              "value" : 128
                           },
                           "duration" : {
                              "text" : "2 mins",
                              "value" : 90
                           },
                           "end_location" : {
                              "lat" : 33.8099333,
                              "lng" : -117.9169697
                           },
                           "html_instructions" : "Turn \u003cb\u003eleft\u003c/b\u003e",
                           "maneuver" : "turn-left",
                           "polyline" : {
                              "points" : "ykjmEv{unUKu@M[GQ]gAg@iA"
                           },
                           "start_location" : {
                              "lat" : 33.8094087,
                              "lng" : -117.9181956
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "492 ft",
                              "value" : 150
                           },
                           "duration" : {
                              "text" : "2 mins",
                              "value" : 110
                           },
                           "end_location" : {
                              "lat" : 33.8097378,
                              "lng" : -117.9154346
                           },
                           "html_instructions" : "Turn \u003cb\u003eright\u003c/b\u003e toward \u003cb\u003eS Harbor Blvd\u003c/b\u003e",
                           "maneuver" : "turn-right",
                           "polyline" : {
                              "points" : "aojmE`tunU\\_@@E@E?e@AaAFSAU?[?_@?k@?M"
                           },
                           "start_location" : {
                              "lat" : 33.8099333,
                              "lng" : -117.9169697
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "82 ft",
                              "value" : 25
                           },
                           "duration" : {
                              "text" : "1 min",
                              "value" : 19
                           },
                           "end_location" : {
                              "lat" : 33.8099608,
                              "lng" : -117.9154365
                           },
                           "html_instructions" : "Turn \u003cb\u003eleft\u003c/b\u003e onto \u003cb\u003eS Harbor Blvd\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eDestination will be on the left\u003c/div\u003e",
                           "maneuver" : "turn-left",
                           "polyline" : {
                              "points" : "{mjmEljunUk@@"
                           },
                           "start_location" : {
                              "lat" : 33.8097378,
                              "lng" : -117.9154346
                           },
                           "travel_mode" : "WALKING"
                        }
                     ],
                     "travel_mode" : "WALKING"
                  },
                  {
                     "distance" : {
                        "text" : "39.7 mi",
                        "value" : 63915
                     },
                     "duration" : {
                        "text" : "1 hour 34 mins",
                        "value" : 5640
                     },
                     "end_location" : {
                        "lat" : 34.049065,
                        "lng" : -118.2600143
                     },
                     "html_instructions" : "Bus towards Downtown LA - 6th - Los Angeles",
                     "polyline" : {
                        "points" : "gojmEdkunU?Uj@A~@@R?J?d@?zD?p@?vC?P?^??g@AkB?c@?_@?aAAiE?q@?kD@K?gD?k@?_CC_@]@eA@I?C@C@C@EFUBS?MAM?Q?YCWEQCMIUGOGKCKEOGQMECICGCGAIAM?E?C?E@I@KBIBKDIDIFEDKL}A~AqAtA[ZQTiClCUVuA|As@p@ONGFGFEHEHCFABA@AFCFAHCFAFAFAPAVGNAFAF?FAlCARCt@?P@j@?BAN?A?J?H@TK?oD@yGBeAAaA?u@?ME_@WIKIQCICQ?U?MJ[BIBGFENIVKZCJ?VBHBBBLJRXFN@H@RARERGPo@n@yFhG{ElFKH_FbFeBfB}DdEeLhKeDrCgCxBaG|E_JbHyAhAg@\\sAjAYTmFdGY^w@bAW\\mKfNcBzBu@dAs@dAe@v@QZe@dAe@jA[dA[hA_@`BQdAM~@KvACj@AjAAtBE`BEdAA\\M|@g@bDKf@UfAUv@Wn@GPeAxBw@hBgFpL_FbLoOh]}BlFaCzEe@jAi@vAYp@]v@m@lAOXi@|@w@hAm@v@yAdBQT[`@m@|@k@~@g@fAMVm@|AM`@i@vBuBdJu@pBa@fAKVcBnDmAhC}AhD_HrO_JlSQ`@uD~HQX[j@Wd@uBzCuClC]\\c@b@e@v@g@z@{A~By@bAQTOPWFU@QAICGGCEEICOAG?G?s@g@@{@?oA@s@?s@?Kj@Kr@QTm@lAGXOj@k@hA?nD?@?z@?f@?lAZ??tA@hA@`@@lAIt@AE?AEAc@?I?UAk@AC}A?QA]?U?cA?UAa@AeA?o@Aa@?s@?a@?aA?aB?s@?y@?}@?K@iBz@?b@?x@?fAAd@AN?b@?b@?b@?b@?l@?b@?b@?r@?P?~@A~@AF?FAN?V?B?@?L?VAB?F?b@?X?b@@h@@X?XAnCAhC?@A@?@ANMH?vAEBF@@@BD@D?vA?Z?jBCZ?f@@dDA\\?hCAzAA@?@ADABCDAHI`@@`@@V?H?fCA?x@?@?h@@b@?H?\\?R?H?n@?d@?l@?zA@zA@fB?t@@^?jB?J@r@?D?V?pA?r@?l@?h@BjB?`A@hC?bB?^?fA?d@?DGh@?X?hE?rADzE?~B?|A?rC?J?~@@\\?nB@b@?lF@fF?tB?\\?fA@`E@d@?F?RJN@xAAjAKZ?J?f@At@?d@?D?`C_A?W?U?Y@a@?m@?g@ByA?W?cB?kAAe@@}@?q@?[?gB?wA@eA?m@@w@@kC@Y?iA@yA?oA?c@?wB?oD@M@A?E?_@?G?K?A?_@@iC@yCBS?cAD_@?sAB]?G?K?[@_B?_B@}@?_@?w@?w@?mAB_D@}@?cA?kA?kA@I?wAAU@oA?yAAe@?c@?c@Aa@?M?K?K?Y?G?A?A?GAE@OAA?]?W?K?C?c@?q@?c@@g@@k@@Q@aBDY?}B@e@@{@?W?{@?G?W?[@eFBqB?mA?Y@aA?oA?qA@C?_@?kA@e@?aCB}C?eA@qA?y@?E?aBAuA?w@@i@?k@?y@?{@DuAHaA@E?W@m@A_@COAUCUCYGi@SKEUK]Qi@]CCYUa@e@gAsAs@}@i@i@GIOOOMMKQMa@Y[Ua@Ye@[CCYQi@_@UOYd@g@v@c@p@U\\]j@]f@g@x@u@lAGJS\\QX[f@ORo@x@o@j@c@XYNKFgAh@iAl@m@V?Z@|@?`B?fB@jA?^?D?j@?B?p@?lB?lB?vA@|AGJAB?B?L?|C?|B@hB@bM?pBHJ?xA@x@?lA?X@zDIFABAB?@?Z?R?f@@dA?L?xA?PAR?j@?H?T@`C@tBHR?P@zB@p@?`C@\\?P?vC@dE?b@?`@?l@@t@?n@?|B?hB?j@@`C?hA?LI^@rC?D?F?FHL@xF?jE@vB?nAG^AZ?h@@hE?V@~B?jA?xA?X?h@?nC@|A?bB?~A@`C?n@@hA?pB@h@?j@?`A?t@?|@?dA@tA@~@?L?\\?j@?h@@J?^?j@?h@?pA?j@?j@?v@?v@?`C?^?\\?H@b@?hB?|A@xC?lC@rB?r@?V?~A?bB@fD?fC@nC?ND??b@W?kBC{PH]AcBAe@?uDEm@?W?M?]?O?W?e@?wEDsDFqH@sE??FU?k@?U?y@?eA@c@?Y?{A?I?s@?k@?q@?sA@_@?g@?s@?Y?@~A?nA?B?jA?~A@dA?bB?lB?z@@hB?V?l@?dC@`C?F?nA?N?hA?hA?B@hC?X?fA?T?~A?v@@r@?^?n@?b@?D?rA?Z@t@?|A?|@?l@?d@?nC@v@?lB?@?D?@?rA@fB?n@CpF@dA?bABxC@\\?|A?r@@\\?v@@bA?N?P?j@?L@lAAbB?pB?v@@hA?nA?l@?h@?P?B?zA?j@?j@@Z?r@?^@`B?j@?P?`@?tC@fC@zA?R@vB@|B?jB?tA?`A?Z?tB?~@@|A?rA?fA?|@?l@@`A?fA?j@?H?~@@z@?j@?nA@z@Al@?|CULyA@a@?}@@cC@eD@i@?S?s@@Q?k@?a@BE@G@E@GFw@AaA?G?K?IIAAA?K?Q?kA?cD@cB@WAUA_@Ga@ICASEs@Um@SQE{@YkA_@UGSGUEOCOCQC[AWAU?UAO?S?O?a@?_A?u@@WAq@?W?i@?_@?qA?yA?w@@_@AmA?uA?y@@_@?o@?w@?Q?kA@YA@\\?j@?R@`@?j@?jA?d@?@?@HT?b@?n@?L?d@?hA?f@?J?l@@fD?N?l@?@?d@AzCAxG?tAKT?B?@?B?n@?fA?TBz@Ar@?z@?z@?D?j@?vB?nA?`C?h@?j@?j@?tD?~@?h@BHHR?tA?jBAjC?`@@vEM\\@dBAzB?XXAh@??MdE?~E@X?BdFBzCEx@EbC?tAF`E`@?^A~@?\\?X?v@?BzAd@?B?bCB?}AcCAm@?w@?Y?]?_A?_@@a@?f@nCBRDTJp@Fh@DTTvBJzA@rG?~ACbECvGE~GAhCAx@?B?rBAdE?tBDjFDpB?XRvJJvENvFBVLpYt@tX?p@?~Q@lF@ZFjMDnFGzJC~D@tD@fB@t@DnABrBDjCBlB?x@DrFHfGHxDFfBTnI^tK\\|JFfATnFdAnX?JBl@HlBPrBHx@l@xEh@xDP`BJrANrCFnG@jE@h@@bA?`CBfHAxEAhCIpDEnAG`BGlASnDKnAIv@SbB[~BSlA]pBe@dCOr@I\\G\\YhAKh@GXELAJWjAIVER}AhHq@fDY|Ao@zDIb@Mt@Kj@kAvFmAxE}@hDQp@cAdDY~@e@jA{BlFgCpGmAvC{AvDK^IZkAlDaBxEcBfFm@|AM\\O^cDdKyB|GQb@O`@oDvKOb@O`@uAzD_GrPiA~C_ApCGR}AzE{C`JoB~FaGdQm@lBcApDc@nBOv@OlAg@~DQtASbCI|BBbC@d@D|AFbBb@xE@LFd@Fp@rCfWDx@Bf@Bp@@^J|DJvG@tB@|CA|CKbLEfBKrBGlAEjA?XAZw@j]SlJAh@Cf@CfB?bB@z@DtBJrCB`@@b@Bj@B~@jAhQJvATnCBd@Ht@TfKRdLb@|S@h@?j@AjNAbA@xA?rOAjEAj@AbAClAEnAI`BGz@OrBSrBKx@_@jCKx@Iv@e@pCQ~AGz@E^BjABdA@z@Az@AHAZGd@Id@Ih@Md@GPKXIPKNW\\Y`@MNwAdB_@^m@l@YTMHKH_@Ne@POHOFMDQJeAG{AIoDSoBKaAEQCYAoAGmFUyBIaAEcBAeAGq@BqABc@Aa@AqB?oB?cDBuCAgC@U?Q?gAC{FBi@CiAIgFYc@Ca@EmAIi@GsBMi@Cm@AgB@iABqAHmAN_@DM@oAR{BXmANaCXkGl@[DWDiCZuCX}AJ_AB_A@oF@U?I@qQE[?E?W?W?E??KM?M?eJDc@?{C@}DBQ?Q?iIDwD@}A?]?I?m@?i@?k@?W?{@?q@?eB?oA?iA?eA?g@AIAK?_@?K?{I?c@?aI?g@?i@?}L@yB?eBAoB?YAG@I?aDAiFKkFYc@Ae@C}BIwDIuD?gDE[?U?uLQKJA@A@E@IAI@W?u@?E?gA@a@?YAo@?c@?o@?{@@a@@S?i@B}@@SCuAB_@@a@?c@@_@?O?S?c@?c@?_@CA?a@CW?c@@c@?c@?E?aA@gA?a@?a@?c@@c@@]@E?[@G?c@Bg@@c@@O@S?C?SBc@@c@@W@K?c@Bc@BM?U@WBK?K@I?S@K@C?c@@q@BI@E?A?Q@M?U@_@@a@@c@Bc@BU@W?gAFeAFq@Bo@Ba@BO@S@_@@G@c@@e@Ba@@k@@[?o@BE?[?Q@Q?c@@c@AQAQAc@Au@?c@?Q?O@A?c@AgA?_@A_@Cc@?c@?e@?e@AgABC?c@@I?W@W?K@c@@KAC?G?WAc@?[?G?c@?S?c@?[?G?oCCeCA[?k@?k@Ao@Am@AOAYAG?e@Aa@AEA]?e@AA?_@?q@AWAu@?o@AgAA}@?EAO?E?C?YAq@AUAG?O?g@Cs@EK?]E[Cc@Aa@Ga@EMCWEUEGAa@IKAk@MKCGAYIIAa@MICc@Ma@M_@KAA[KICELOIWGOEIEIGa@SQIq@c@a@YoAy@cH{Dc@UWOa@ScQuHYOcAe@uFiCk@[i@Wi@Wi@Wg@Qg@S{@UYGM^KTIVkB]cACC??RG?IAI?I?K?I?G@c@@A?_@BY@y@?O@K?G@YBEA_@UOIOIk@[m@[c@UIEECECMGQM_@Si@[iBcA[QyAw@SK_@UECm@[c@WKGUMw@c@GC{@e@CCWMQMa@S]QIEUMEC}@g@c@Ua@Su@c@oBgAIECCC?OMA?IGGCAAECECcAk@MIA?SKa@SAAKE{@e@c@Uq@]YSiAm@OI[SkCyAUMWOWMSM_B}@SOECmAgA_@]UUWWQOc@a@WU_BwA][iBaBk@i@qAkAcB_BMMSQaAaAy@y@oAgASQgB}Am@i@aA_As@o@}AyAGGKGGCCCCCCCAGwAkAc@a@w@s@YWCCWUi@g@q@m@NW"
                     },
                     "start_location" : {
                        "lat" : 33.8099601,
                        "lng" : -117.9155484
                     },
                     "transit_details" : {
                        "arrival_stop" : {
                           "location" : {
                              "lat" : 34.049065,
                              "lng" : -118.2600143
                           },
                           "name" : "Figueroa & 7th St"
                        },
                        "arrival_time" : {
                           "text" : "5:34am",
                           "time_zone" : "America/Los_Angeles",
                           "value" : 1566563640
                        },
                        "departure_stop" : {
                           "location" : {
                              "lat" : 33.8099601,
                              "lng" : -117.9155484
                           },
                           "name" : "Disneyland"
                        },
                        "departure_time" : {
                           "text" : "4:00am",
                           "time_zone" : "America/Los_Angeles",
                           "value" : 1566558000
                        },
                        "headsign" : "Downtown LA - 6th - Los Angeles",
                        "line" : {
                           "agencies" : [
                              {
                                 "name" : "Metro - Los Angeles",
                                 "phone" : "1 (323) 466-3876",
                                 "url" : "http://www.metro.net/"
                              }
                           ],
                           "color" : "#21bdc6",
                           "name" : "Metro Express Line",
                           "short_name" : "460",
                           "text_color" : "#ffffff",
                           "vehicle" : {
                              "icon" : "//maps.gstatic.com/mapfiles/transit/iw2/6/bus2.png",
                              "name" : "Bus",
                              "type" : "BUS"
                           }
                        },
                        "num_stops" : 63
                     },
                     "travel_mode" : "TRANSIT"
                  },
                  {
                     "distance" : {
                        "text" : "0.1 mi",
                        "value" : 171
                     },
                     "duration" : {
                        "text" : "3 mins",
                        "value" : 159
                     },
                     "end_location" : {
                        "lat" : 34.048634,
                        "lng" : -118.258682
                     },
                     "html_instructions" : "Walk to 7th St / Metro Center",
                     "polyline" : {
                        "points" : "cfynEltxpUg@c@KIx@}AIGhBaC"
                     },
                     "start_location" : {
                        "lat" : 34.0491438,
                        "lng" : -118.260072
                     },
                     "steps" : [
                        {
                           "distance" : {
                              "text" : "102 ft",
                              "value" : 31
                           },
                           "duration" : {
                              "text" : "1 min",
                              "value" : 26
                           },
                           "end_location" : {
                              "lat" : 34.0493961,
                              "lng" : -118.259836
                           },
                           "html_instructions" : "Head \u003cb\u003enortheast\u003c/b\u003e on \u003cb\u003eS Figueroa St\u003c/b\u003e toward \u003cb\u003e7th St\u003c/b\u003e",
                           "polyline" : {
                              "points" : "cfynEltxpUg@c@KI"
                           },
                           "start_location" : {
                              "lat" : 34.0491438,
                              "lng" : -118.260072
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "187 ft",
                              "value" : 57
                           },
                           "duration" : {
                              "text" : "1 min",
                              "value" : 49
                           },
                           "end_location" : {
                              "lat" : 34.0491138,
                              "lng" : -118.259372
                           },
                           "html_instructions" : "Turn \u003cb\u003eright\u003c/b\u003e onto \u003cb\u003e7th St\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eDestination will be on the left\u003c/div\u003e",
                           "maneuver" : "turn-right",
                           "polyline" : {
                              "points" : "wgynE~rxpUx@}A"
                           },
                           "start_location" : {
                              "lat" : 34.0493961,
                              "lng" : -118.259836
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "272 ft",
                              "value" : 83
                           },
                           "duration" : {
                              "text" : "1 min",
                              "value" : 84
                           },
                           "end_location" : {
                              "lat" : 34.048634,
                              "lng" : -118.258682
                           },
                           "html_instructions" : "Take entrance \u003cspan class=\"location\"\u003e7th Street / Metro Center Station - 7th & Figueroa Elevator\u003c/span\u003e",
                           "polyline" : {
                              "points" : "gfynExoxpUhBaC"
                           },
                           "start_location" : {
                              "lat" : 34.04916,
                              "lng" : -118.259331
                           },
                           "travel_mode" : "WALKING"
                        }
                     ],
                     "travel_mode" : "WALKING"
                  },
                  {
                     "distance" : {
                        "text" : "10.7 mi",
                        "value" : 17279
                     },
                     "duration" : {
                        "text" : "20 mins",
                        "value" : 1200
                     },
                     "end_location" : {
                        "lat" : 34.1392072,
                        "lng" : -118.3625352
                     },
                     "html_instructions" : "Subway towards North Hollywood Station",
                     "polyline" : {
                        "points" : "}bynEvkxpUQOMTgE|JwHlQsMx^iDdKOd@qJlYo@jBw@fBcBrDmHlKmCpEiJ~Vg@bBI|B@rP_@vDq@jDk@nB}@bBYQk@lAa@t@S^MTMRQTY\\YXYTYRe@XUL]Nc@Nm@R{@Rg@Ji@D[Bm@@c@?Y?YC_@E[G_@I_@MUIWMSKSKQKOMs@a@_@QWM[OWIWIUE]IWEQCYES?WC[AU?wA?_C?gB?{B?kB@eC?oC?iN@gA??TM?oF?wE?uE@i@?i@?{F@_H@_H@cI@wNBiF?sE@eA?Q?Q?mOCqWEc@AW@iNVwDfC?b[Avz@?j@?nL?l@J`oABrU@|FPxhAOtBFC@bB@rL?hAAt@Aj@Aj@Gv@Ed@G^EZG^I`@Kb@Ux@Uj@Ob@OXSb@U^[d@W^UVWXc@`@c@\\c@Xo@^m@Zk@Vq@Zq@Zq@Xk@Vw@\\w@^o@XqAj@aAd@{@^mAh@}@`@q@Zq@X}@`@}Ar@m@Vy@^w@^_A`@gBv@yAp@sAh@mAj@cA`@yB~@yCnAaCdA{BbAwCpA_CfAwCpAaCfA}CrAwAp@gBv@wCrAmAh@}@`@mEnBu@\\kD|AeDxAuEtBqD~AgD|AqBz@yAp@gBx@aCdAqB~@_FxB}CtAaCdAwCrAmEnBe@R_@Nk@Ro@Pi@Jc@FYDa@Bc@@g@@q@Ao@Ca@E{@Ku@MqB]wB]yAW}Ba@QCHy@"
                     },
                     "start_location" : {
                        "lat" : 34.048634,
                        "lng" : -118.258682
                     },
                     "transit_details" : {
                        "arrival_stop" : {
                           "location" : {
                              "lat" : 34.1392072,
                              "lng" : -118.3625352
                           },
                           "name" : "Universal City/Studio City"
                        },
                        "arrival_time" : {
                           "text" : "6:02am",
                           "time_zone" : "America/Los_Angeles",
                           "value" : 1566565320
                        },
                        "departure_stop" : {
                           "location" : {
                              "lat" : 34.048634,
                              "lng" : -118.258682
                           },
                           "name" : "7th St / Metro Center"
                        },
                        "departure_time" : {
                           "text" : "5:42am",
                           "time_zone" : "America/Los_Angeles",
                           "value" : 1566564120
                        },
                        "headsign" : "North Hollywood Station",
                        "line" : {
                           "agencies" : [
                              {
                                 "name" : "Metro - Los Angeles",
                                 "phone" : "1 (323) 466-3876",
                                 "url" : "http://www.metro.net/"
                              }
                           ],
                           "color" : "#ee1d23",
                           "name" : "Metro Red Line",
                           "text_color" : "#ffffff",
                           "url" : "http://www.metro.net/around/rail/red-line/",
                           "vehicle" : {
                              "icon" : "//maps.gstatic.com/mapfiles/transit/iw2/6/us-losangeles-lacmta.png",
                              "name" : "Subway",
                              "type" : "SUBWAY"
                           }
                        },
                        "num_stops" : 9
                     },
                     "travel_mode" : "TRANSIT"
                  },
                  {
                     "distance" : {
                        "text" : "0.5 mi",
                        "value" : 786
                     },
                     "duration" : {
                        "text" : "13 mins",
                        "value" : 755
                     },
                     "end_location" : {
                        "lat" : 34.1364652,
                        "lng" : -118.3569535
                     },
                     "html_instructions" : "Walk to 100 Universal City Plaza, Universal City, CA 91608, USA",
                     "polyline" : {
                        "points" : "ayjoEztlqUi@FEFBBD@HBf@cBDOJ_@d@iBHq@@a@?g@Gc@AEAa@Au@?I?a@?qADaC@k@?g@Nm@TgAFUBGBEJODIFG?AFGDCX]b@YFEDC@ADCDEHEDCDEFEFEFEPKBA@AFCDALCVERAD@BBDB@@@D@Bt@VJDRDN?LAHC"
                     },
                     "start_location" : {
                        "lat" : 34.1392072,
                        "lng" : -118.3625352
                     },
                     "steps" : [
                        {
                           "distance" : {
                              "text" : "79 ft",
                              "value" : 24
                           },
                           "duration" : {
                              "text" : "1 min",
                              "value" : 25
                           },
                           "end_location" : {
                              "lat" : 34.1394241,
                              "lng" : -118.3625845
                           },
                           "html_instructions" : "Take exit \u003cspan class=\"location\"\u003eUniversal / Studio City Station - North Elevator\u003c/span\u003e",
                           "polyline" : {
                              "points" : "ayjoEztlqUi@F"
                           },
                           "start_location" : {
                              "lat" : 34.1392072,
                              "lng" : -118.3625352
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "52 ft",
                              "value" : 16
                           },
                           "duration" : {
                              "text" : "1 min",
                              "value" : 12
                           },
                           "end_location" : {
                              "lat" : 34.1393516,
                              "lng" : -118.3626746
                           },
                           "html_instructions" : "Head \u003cb\u003esouth\u003c/b\u003e toward \u003cb\u003eCampo De Cahuenga\u003c/b\u003e",
                           "polyline" : {
                              "points" : "qzjoEjulqUBBD@HB"
                           },
                           "start_location" : {
                              "lat" : 34.1394486,
                              "lng" : -118.3626236
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "180 ft",
                              "value" : 55
                           },
                           "duration" : {
                              "text" : "1 min",
                              "value" : 42
                           },
                           "end_location" : {
                              "lat" : 34.1391195,
                              "lng" : -118.3620924
                           },
                           "html_instructions" : "Turn \u003cb\u003eleft\u003c/b\u003e onto \u003cb\u003eCampo De Cahuenga\u003c/b\u003e",
                           "maneuver" : "turn-left",
                           "polyline" : {
                              "points" : "}yjoEtulqUf@cBDO"
                           },
                           "start_location" : {
                              "lat" : 34.1393516,
                              "lng" : -118.3626746
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "440 ft",
                              "value" : 134
                           },
                           "duration" : {
                              "text" : "2 mins",
                              "value" : 110
                           },
                           "end_location" : {
                              "lat" : 34.138809,
                              "lng" : -118.3607778
                           },
                           "html_instructions" : "Continue onto \u003cb\u003eUniversal Hollywood Dr\u003c/b\u003e",
                           "polyline" : {
                              "points" : "oxjoE`rlqUJ_@d@iBHq@@a@?g@"
                           },
                           "start_location" : {
                              "lat" : 34.1391195,
                              "lng" : -118.3620924
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "0.3 mi",
                              "value" : 440
                           },
                           "duration" : {
                              "text" : "8 mins",
                              "value" : 476
                           },
                           "end_location" : {
                              "lat" : 34.1374282,
                              "lng" : -118.3567265
                           },
                           "html_instructions" : "Slight \u003cb\u003eleft\u003c/b\u003e to stay on \u003cb\u003eUniversal Hollywood Dr\u003c/b\u003e",
                           "maneuver" : "turn-slight-left",
                           "polyline" : {
                              "points" : "qvjoEzilqUGc@AEAa@Au@?I?a@?qADaC@k@?g@Nm@TgAFUBGBEJODIFG?AFGDCX]b@YFEDC@ADCDEHEDCDEFEFEFEPKBA@AFCDALC"
                           },
                           "start_location" : {
                              "lat" : 34.138809,
                              "lng" : -118.3607778
                           },
                           "travel_mode" : "WALKING"
                        },
                        {
                           "distance" : {
                              "text" : "384 ft",
                              "value" : 117
                           },
                           "duration" : {
                              "text" : "2 mins",
                              "value" : 90
                           },
                           "end_location" : {
                              "lat" : 34.1364652,
                              "lng" : -118.3569535
                           },
                           "html_instructions" : "Continue onto \u003cb\u003eCoral Dr\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eDestination will be on the right\u003c/div\u003e",
                           "polyline" : {
                              "points" : "}mjoEppkqUVERAD@BBDB@@@D@Bt@VJDRDN?LAHC"
                           },
                           "start_location" : {
                              "lat" : 34.1374282,
                              "lng" : -118.3567265
                           },
                           "travel_mode" : "WALKING"
                        }
                     ],
                     "travel_mode" : "WALKING"
                  }
               ],
               "traffic_speed_entry" : [],
               "via_waypoint" : []
            }
         ],
         "overview_polyline" : {
            "points" : "gljmEp`vnUYyAf@_A_AkDg@iA\\_@BKBqC?uBk@Vj@WdC@vK?^??g@AoCA}I@kKC_DcBBUD}@HwCg@yAm@gAHyQdRs@|CGpH@h@{OBwB?m@][y@NiAr@c@hABl@x@CdAmOxPoOzOu]tZuPvMyI~J_QfUqC~E}B|Ho@hGOhJcAfHkA`EeQx`@e]nv@uItLoC|FcFpRo]fw@{FjLkGhHaA`AcFvHoAp@g@SKi@?s@g@@kC@gB?W~AwAhDk@hA?nD?|@?tBZ??tABjBGbCk@I_@Ak@AC}AAo@?yACmDAgM@uB~A?vDCxH?lEErOEtBKNFpREzCSjE?@hC?dHFjMAr`@Lrw@LpAKlEAhC?`C_A?m@?qCDaHAeNDgZHaz@ZkYLs`@Ng^TsBKeBe@aB_AwDmEoD{CeBkA_Ao@aA|AuBbDyBpDmBnCyB|AqCvAm@r@BrI?zLEdONx^KnD?zIPzNBtTEhPJvGErL@fRDvUFrXDtc@B~UDb@cCCcVDuJEi^VcK@mE@mA?@nD@tF@vJ@~NDzXL|bALbbABvL?hBUjD}HD}JHcDH{GIqDImBg@wGmBeDMuX@yC?eB?@hA@lDH~@?rHAxWKlD@`NBlPF`JI|JAtCbAAdEMxF@C~PFvG`AAnD?BzAd@?fCB?}AcCAeB?wC@a@?f@nCHh@n@hFHrSMzV\\bd@l@xh@t@fZ@lYFra@@rOl@ld@~D~hAh@hItBhQZxSBfUYlNq@dKiDxS}A~GiIhb@aFzQcIjS_Oxa@}Obf@qVts@cNdb@kBtLe@xEE`Gr@`MtD``@ZhV_@|WaBzq@XbSpBpZv@h[b@~e@A|ZcArSgCdSFhGUpBk@zBgA`BsDbEsAx@sAh@cJYwUeAml@EmSqAqHLkEj@k\\rDmMRmSC{@?MKsJDoKDqRFqH?cLCwi@@mSOsLi@qPOuN?_TH_MA_Yp@k]lAm[E}d@a@{Km@yHgBgA]UBg@MgAk@gMsHa`@kQeDyAcBi@YGM^Ul@oDa@UPwBFoBBgAQsDoBwPkJcOgIsYePii@wf@eIiH?Ks@m@n@eBhBaCQOuErKwHlQsMx^yDjLaLx\\{CzG{L~QqKbZGpTqAbJiBrEYQk@lAu@tAgA|AsB|AeC`AiDh@eCA{Be@kE}BaEkAwLK}N@qP@MTgU@eZDqz@BuXGaOXwDfC?b[Ab|@NrtBJhtAOlYgBjH_EvFgJ|Eg]pOax@v]y`A~b@w]pOwCj@_DDeSyCG}@o@NHDp@_B`AkEGsBBkLnA{DzAqAdCkA~Aj@fAD"
         },
         "summary" : "",
         "warnings" : [
            "Walking directions are in beta. Use caution – This route may be missing sidewalks or pedestrian paths."
         ],
         "waypoint_order" : []
      }
   ],
   "status" : "OK"
}');
?>